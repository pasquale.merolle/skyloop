## Skyloop App
#### Front End Hiring Test App

Welcome to skyloop app, an hiring test from fastloop, based on skyscanner API and figma [mockup](https://www.figma.com/file/bmVI42Dg9bHsh5LGWOFRXk/Mockup?node-id=0%3A1)

to reduce bundle size and grant manutenibility avoiding external dependencies issues, this app does not use any other dependencies beside [React](https://it.reactjs.org/) and [Redux Toolkit](https://redux-toolkit.js.org/)

## Available Scripts

To install the app dependencies run:

#### `yarn`

*NB: you need [yarn](https://yarnpkg.com/) package manager (v1.22 min) 

----

To start the app in development mode (watch) run:

#### `yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

----

To launch the tests in watch mode run:
#### `yarn test`

you can launch a single test using the test name, ex:

#### `yarn test session`

----

To build the app for production environment, optimizing the build for best performance, run:

#### `yarn build`

The app is ready to be deployed!

## Typescript

This app uses [Typescript](https://www.typescriptlang.org/) as static type checker

----

### Disclaimer

Due to mockup resolution (1152px) maybe you can try to zoom in the browser window to have a better experience. This app is not ready for Mobile devices 