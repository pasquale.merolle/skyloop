import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { Itinerary, ListItem, SessionPayload } from "../beans";
import { mapPlacesToListItems, mapPollToItineraies } from "../utils";
import { API_URL } from "../utils/constants";
import { API } from "../beans/api";

export const skyscannerApi = createApi({
  reducerPath: "skyscannerApi",
  baseQuery: fetchBaseQuery({ baseUrl: API_URL }),
  endpoints: (builder) => ({
    getPlaces: builder.query<ListItem[] | undefined, string>({
      query: (query) => `/places?query=${query}`,
      transformResponse: mapPlacesToListItems,
    }),
    postSession: builder.mutation<API.Session | {}, Partial<SessionPayload>>({
      query: (body) => ({
        url: `/session`,
        method: "POST",
        body,
        headers: {
          "content-type": "application/json",
        },
        redirect: "follow",
      }),
    }),
    getItineraries: builder.query<Itinerary[] | undefined, string>({
      query: (sessionUrl) => ({
        url: `${API_URL}/session?sessionUrl=${sessionUrl}`,
      }),
      transformResponse: mapPollToItineraies,
    }),
  }),
});

export const {
  useGetPlacesQuery,
  useGetItinerariesQuery,
  usePostSessionMutation,
} = skyscannerApi;
export const getPlaces = skyscannerApi.endpoints.getPlaces.initiate;
