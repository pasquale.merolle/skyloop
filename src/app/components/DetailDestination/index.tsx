import React from "react";

import styles from "./index.module.css";

type Props = {
  destination: string;
};

export function DetailDestination(props: Props) {
  return <div className={styles.wrap}>{props.destination}</div>;
}
