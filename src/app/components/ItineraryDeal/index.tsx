import React from "react";
import { Itinerary } from "../../beans";
import { currencyFormatter } from "../../utils";
import { SearchButton } from "../SearchButton";
import styles from "./index.module.css";

type Props = {
  itinerary: Itinerary;
  onSelect: () => void;
};

export function ItineraryDeal(props: Props) {
  const { itinerary, onSelect } = props;

  const renderPrice = () => {
    return itinerary.prices.map((price) => price);
  };

  return (
    <div className={styles.wrap}>
      <span className={styles.label}>
        {renderPrice().length}deal{renderPrice().length > 1 ? "s" : ""}
      </span>
      <span className={styles.price}>
        {currencyFormatter(renderPrice()[0])}
      </span>
      <SearchButton onClick={onSelect} label="Select" variant="button" />
    </div>
  );
}
