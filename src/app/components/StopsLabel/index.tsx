import React from "react";
import styles from "./index.module.css";
export function StopLabel() {
  return <span className={styles.label}>Stops</span>;
}
