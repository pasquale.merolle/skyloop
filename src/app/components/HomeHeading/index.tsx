import React from 'react';
import styles from './index.module.css';

export function HomeHeading() {


  return (
    <h1 className={styles.heading}>
      Let the journey begin
    </h1>
  );
}
