import React from "react";
import { Bound } from "../../beans";
import { convertMinsToHrsMins, renderTime } from "../../utils/tools";
import styles from "./index.module.css";

type Props = {
  bound: Bound;
};

export function ItineraryBound(props: Props) {
  const renderStops = (stops: { code: string; name: string }[]) => {
    return stops.map((stop) => (
      <span key={stop.code} className={styles["stop-airport"]}>
        {stop.name}
      </span>
    ));
  };

  const { arrival, departure, info } = props.bound;

  return (
    <div className={styles.wrap}>
      <div className={styles.departure}>
        <span className={styles.time}>{renderTime(departure.time)}</span>
        <span className={styles.airport}>{departure.airport.code}</span>
      </div>
      <div className={styles.info}>
        <span className={styles.duration}>
          {convertMinsToHrsMins(info.duration)}
        </span>
        {info.stops.length ? <span className={styles.redpoint}></span> : <></>}
        <div>
          <span
            className={info.stops.length ? styles["stops"] : styles["direct"]}
          >
            {info.stops.length
              ? `${info.stops.length}stop${info.stops.length > 1 ? "s" : ""}`
              : "Direct"}
          </span>
          {info.stops.length ? (
            <div className={styles.places}>{renderStops(info.stops)}</div>
          ) : (
            <></>
          )}
        </div>
      </div>
      <div className={styles.arrival}>
        <span className={styles.time}>{renderTime(arrival.time)}</span>
        <span className={styles.airport}>{arrival.airport.code}</span>
      </div>
    </div>
  );
}
