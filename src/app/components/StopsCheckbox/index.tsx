import React from "react";
import { Stop } from "../../beans";
import { currencyFormatter } from "../../utils";
import styles from "./index.module.css";

type Props = {
  id: Stop;
  name: string;
  label: string;
  price: number;
  checked: boolean;
  onChange: (id: Stop) => void
};

export function StopCheckbox(props: Props) {
  const { id, name, label, price, onChange, checked } = props;
  
  return (
    <div className={styles.wrap}>
      <input
        onChange={() => onChange(id)}
        type="checkbox"
        id={id.toString()}
        name={name}
        checked={checked}
      ></input>
      <label htmlFor={id.toString()}>
        <span>{label}</span>
        <span>{!isNaN(price) && currencyFormatter(price)}</span>
      </label>
    </div>
  );
}
