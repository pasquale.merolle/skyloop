import React from "react";
import { Itinerary } from "../../beans";
import styles from "./index.module.css";

type Props = {
  itinerary: Itinerary;
};

export function ItineraryCompany(props: Props) {
  const renderCompanyName = (): string => {
    const outBoundName = props.itinerary.outBound.flights.map(
      (flight) => flight.name
    );
    const inBoundName = props.itinerary.inBound?.flights.map(
      (flight) => flight.name
    );
    const res = [...outBoundName];
    if (inBoundName) res.concat(inBoundName);
    return res.filter((item, pos) => res.indexOf(item) === pos).join("+");
  };

  return (
    <div className={styles.wrap}>
      <span>{renderCompanyName()}</span>
    </div>
  );
}
