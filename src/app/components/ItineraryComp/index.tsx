import React from "react";
import { useHistory } from "react-router-dom";
import { Itinerary } from "../../beans";
import { ItineraryCompany } from "../ItineraryCompany";
import { ItineraryDeal } from "../ItineraryDeal";
import { ItineraryRow } from "../ItineraryRow";
import styles from "./index.module.css";

type Props = {
  itinerary: Itinerary;
};

export function ItineraryComp(props: Props) {
  const history = useHistory();
  const { itinerary } = props;

  const handleOnSelect = () => {
    // INFO: workaround for history.push Warning: refers to https://github.com/downshift-js/downshift/issues/874
    setTimeout(() => {
      if (history.location.pathname !== `/results/${itinerary.id}`) history.push(`/results/${itinerary.id}`);
    });
  };

  return (
    <div className={styles.wrap}>
      <ItineraryCompany itinerary={itinerary} />
      <div className={styles.rows}>
        <ItineraryRow itinerary={itinerary} />
      </div>
      <ItineraryDeal itinerary={itinerary} onSelect={handleOnSelect} />
    </div>
  );
}
