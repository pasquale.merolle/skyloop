import React from "react";
import { Itinerary } from "../../beans";
import { ItineraryBound } from "../ItineraryBound";

type Props = {
  itinerary: Itinerary;
};

export function ItineraryRow(props: Props) {
  return (
    <>
      <ItineraryBound bound={props.itinerary.outBound} />
      {props.itinerary.inBound && (
        <ItineraryBound bound={props.itinerary.inBound} />
      )}
    </>
  );
}
