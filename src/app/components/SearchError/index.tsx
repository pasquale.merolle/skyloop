import React from "react";
import { FormCheck } from "../../beans";

import styles from "./index.module.css";

type Props = {
  check: FormCheck[];
};

export function SearchError(props: Props) {
  const { check } = props;

  return (
    <div className={styles.error}>
      {check.map((control) => (
        <span key={control.message}>{control.message}</span>
      ))}
    </div>
  );
}
