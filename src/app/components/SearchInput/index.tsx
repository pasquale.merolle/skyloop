import React, { ChangeEvent } from "react";
import { FormCheck } from "../../beans";

import styles from "./index.module.css";

type Props = {
  name: string;
  value?: string;
  label: string;
  variant: string;
  checked?: boolean;
  disabled?: boolean;
  onChange: (value: string) => void;
  onFocus?: () => void;
  children?: JSX.Element;
  showSelect?: boolean;
  setShowSelect?: (show: boolean) => void;
  check: FormCheck[];
};

export function SearchInput(props: Props) {
  const {
    value,
    variant,
    name,
    label,
    checked,
    disabled,
    check,
    onFocus,
    onChange,
  } = props;

  const selectStyle = () => (variant === "radio" ? "option" : "default");

  const handleOnChange = (e: ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    onChange(value);
    props.setShowSelect && props.setShowSelect(true);
  };

  const checkValidity = (): boolean => {
    let isValid = true;
    if (check.length)
      check.forEach((control) => {
        if (control.field === name) isValid = false;
      });
    return isValid;
  };

  return (
    <div className={styles[selectStyle()]}>
      <label htmlFor={value}>{label}</label>
      <input
        className={!checkValidity() ? styles.invalid : ""}
        type={variant}
        onChange={(e) => handleOnChange(e)}
        id={value}
        name={name}
        value={value}
        checked={checked}
        disabled={disabled}
        onFocus={() => onFocus && onFocus()}
      />
      <div className={styles.circle}></div>
      {props.showSelect && props.children}
    </div>
  );
}
