import React from "react";

import styles from "./index.module.css";

type Props = {
  label: string;
  onClick?: () => void
  variant: "submit" | "button"
};

export function SearchButton(props: Props) {
  const  {label, onClick, variant} = props
  return (
    <button 
      className={styles[variant]}
      onClick={() => onClick && onClick()}
      >
      {label}
      <svg
        width="11"
        height="13"
        viewBox="0 0 11 13"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M10.5689 7.03032C10.8618 6.73743 10.8618 6.26256 10.5689 5.96966L5.7959 1.19669C5.50301 0.903798 5.02814 0.903798 4.73524 1.19669C4.44235 1.48958 4.44235 1.96446 4.73524 2.25735L8.97788 6.49999L4.73524 10.7426C4.44235 11.0355 4.44235 11.5104 4.73524 11.8033C5.02814 12.0962 5.50301 12.0962 5.7959 11.8033L10.5689 7.03032ZM0.538544 7.24999H10.0385V5.74999H0.538544V7.24999Z"
          fill="white"
        />
      </svg>
    </button>
  );
}
