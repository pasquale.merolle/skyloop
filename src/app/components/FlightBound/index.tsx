import React from "react";
import { Bound } from "../../beans";
import { ItineraryBound } from "../ItineraryBound";
import { ItinerarySegment } from "../ItinerarySegment";
import styles from "./index.module.css";

type Props = {
  bound: Bound;
};
export function FlightBound(props: Props) {
  const renderCompanyName = (): string => {
    const boundNames = props.bound.flights.map((flight) => flight.name);
    return boundNames
      .filter((item, pos) => boundNames.indexOf(item) === pos)
      .join("+");
  };

  const renderSegments = () => {
    return props.bound.segments.map((segment, i) => (
      <ItinerarySegment
        key={i}
        segment={segment}
        connection={props.bound.connections[i]}
      />
    ));
  };

  return (
    <div className={styles.bound}>
      <div className={styles.header}>
        <div className={styles.company}>
          <span>{renderCompanyName()}</span>
        </div>
        <div className={styles.timetable}>
          <ItineraryBound bound={props.bound} />
        </div>
        <div className={styles.company}></div>
      </div>
      <div className={styles.segments}>{renderSegments()}</div>
    </div>
  );
}
