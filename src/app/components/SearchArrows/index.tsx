import React from "react";

type Props = {
  onClick: () => void;
}

export function SearchArrows(props: Props) {
  return (
    <svg
      width="11"
      height="9"
      viewBox="0 0 11 9"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      onClick={() => props.onClick()}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M3.85102 0L0 2.22339L3.85102 4.44677V2.65128H6.84626V1.7955H3.85102V0Z"
        fill="white"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M6.41837 8.38171L10.2694 6.15833L6.41837 3.93494L6.41837 5.73044L3.42313 5.73044L3.42313 6.58622L6.41837 6.58622L6.41837 8.38171Z"
        fill="white"
      />
    </svg>
  );
}
