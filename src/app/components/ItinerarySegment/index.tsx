import React from "react";
import { Segment } from "../../beans";
import { convertMinsToHrsMins, renderTime } from "../../utils/tools";
import styles from "./index.module.css";

type Props = {
  segment: Segment;
  children?: JSX.Element | JSX.Element[];
  connection?: string;
};

export function ItinerarySegment(props: Props) {
  const { segment, connection } = props;
  return (
    <>
      <div className={styles.wrap}>
        <span className={styles.duration}>
          {convertMinsToHrsMins(segment.duration)}
        </span>
        <div className={styles.flights}>
          <span className={styles.union}></span>
          <div className={styles.flight}>
            <span className={styles.circle}></span>
            <span>{renderTime(segment.departure.time)}</span>
            <span>
              {segment.departure.airport.code} {segment.departure.airport.name}
            </span>
          </div>
          <div className={styles.flight}>
            <span className={styles.circle}></span>
            <span>{renderTime(segment.arrival.time)}</span>
            <span>
              {segment.arrival.airport.code} {segment.arrival.airport.name}
            </span>
          </div>
        </div>
      </div>
      {connection && (
        <div className={styles.connect}>
          <span className={styles.duration}>{connection}</span>
          <span>Connect in Airport</span>
        </div>
      )}
    </>
  );
}
