import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";

export interface BarStatusState {
  barStatus: string;
}

const initialState: BarStatusState = {
  barStatus: "home",
};

export const barStatusSlice = createSlice({
  name: "barStatus",
  initialState,
  reducers: {
    setBarStatus: (state, action: PayloadAction<string>) => {
      state.barStatus = action.payload
    },
  },
});

export const selectBarStatus = (state: RootState) => state.barStatus;

export const { setBarStatus } = barStatusSlice.actions;

export default barStatusSlice.reducer;
