import searchBarReducer, {
  BarStatusState,
  setBarStatus,
} from '.';

describe('counter reducer', () => {
  const initialState: BarStatusState = {
    barStatus: 'home',
  };
  it('should handle initial state', () => {
    expect(searchBarReducer(undefined, { type: 'unknown' })).toEqual({
      barStatus: 'home',
    });
  });

  it('should handle increment', () => {
    const actual = searchBarReducer(initialState, setBarStatus('home'));
    expect(actual.barStatus).toEqual('home');
  });

  it('should handle decrement', () => {
    const actual = searchBarReducer(initialState, setBarStatus('results'));
    expect(actual.barStatus).toEqual('results');
  });

  it('should handle incrementByAmount', () => {
    const actual = searchBarReducer(initialState, setBarStatus('details'));
    expect(actual.barStatus).toEqual('details');
  });
});
