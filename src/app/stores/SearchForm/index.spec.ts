import searchFormReducer, {
  SearchFormState,
  add,
} from '.';

const now = new Date();
const oneDayFromNow = now.setDate(now.getDate() + 1);
const sevenDaysFromNow = now.setDate(now.getDate() + 7);

describe('search form reducer', () => {
  const initialState: SearchFormState = {
    type: "return",
    from: "",
    to: "",
    departDate: new Date(oneDayFromNow).toISOString().split("T")[0],
    returnDate: new Date(sevenDaysFromNow).toISOString().split("T")[0]
  };
  it('should handle initial state', () => {
    expect(searchFormReducer(undefined, { type: 'unknown' })).toEqual({
      type: "return",
      from: "",
      to: "",
      departDate: new Date(oneDayFromNow).toISOString().split("T")[0],
      returnDate: new Date(sevenDaysFromNow).toISOString().split("T")[0]
    });
  });

  it('should handle add', () => {
    const actual = searchFormReducer(initialState, add({
      type:"return",
      from:"Milan Malpensa (MXP)",
      to:"London (LOND)",
      departDate:"2021-09-30",
      returnDate:"2021-10-03"
    }));
    expect(actual).toEqual({
      type:"return",
      from:"Milan Malpensa (MXP)",
      to:"London (LOND)",
      departDate:"2021-09-30",
      returnDate:"2021-10-03"
    });
  });

  it('should handle empty add', () => {
    const actual = searchFormReducer(initialState, add({
      type: "return",
      from: "",
      to: "",
      departDate: new Date(oneDayFromNow).toISOString().split("T")[0],
      returnDate: new Date(sevenDaysFromNow).toISOString().split("T")[0]
    }));
    expect(actual).toEqual(initialState);
  });

});
