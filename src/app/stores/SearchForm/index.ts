import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";

export interface SearchFormState {
  type: "oneway" | "return";
  from: string;
  to: string;
  departDate: string;
  returnDate?: string;
}

const now = new Date();
const oneDayFromNow = now.setDate(now.getDate() + 1);
const sevenDaysFromNow = now.setDate(now.getDate() + 7);

const initialState: SearchFormState = {
  type: "return",
  from: localStorage.getItem("from") || "",
  to: localStorage.getItem("to") || "",
  departDate:
    localStorage.getItem("departDate") ||
    new Date(oneDayFromNow).toISOString().split("T")[0],
  returnDate:
    localStorage.getItem("returnDate") ||
    new Date(sevenDaysFromNow).toISOString().split("T")[0],
};

export const searchFormSlice = createSlice({
  name: "searchform",
  initialState,
  reducers: {
    add: (state, action: PayloadAction<SearchFormState>) => {
      const { from, to, departDate, returnDate } = action.payload;
      localStorage.setItem("from", from);
      localStorage.setItem("to", to);
      localStorage.setItem("departDate", departDate);
      returnDate && localStorage.setItem("returnDate", returnDate);
      return {
        ...state,
        ...action.payload,
      };
    },
  },
});

export const selectSearchForm = (state: RootState) => state.searchForm;

export const { add } = searchFormSlice.actions;

export default searchFormSlice.reducer;
