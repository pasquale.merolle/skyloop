import sessionReducer, {
  SessionState,
  setSessionUrl,
} from '.';

describe('session reducer', () => {
  const initialState: SessionState = {
    sessionUrl: '',
  };
  it('should handle initial state', () => {
    expect(sessionReducer(undefined, { type: 'unknown' })).toEqual({
      sessionUrl: '',
    });
  });

  it('should set session url', () => {
    const actual = sessionReducer(initialState, setSessionUrl({sessionUrl: 'https://www.mockedurl.com'}));
    expect(actual.sessionUrl).toEqual('https://www.mockedurl.com');
  });

  it('should set empty url', () => {
    const actual = sessionReducer(initialState, setSessionUrl({}));
    expect(actual.sessionUrl).toEqual('');
  });
});
