import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";

export interface SessionState {
  sessionUrl: string;
}

const initialState: SessionState = {
  sessionUrl: "",
};

export const sessionSlice = createSlice({
  name: "session",
  initialState,
  reducers: {
    setSessionUrl: (state, action: PayloadAction<SessionState | {}>) => {
      if (Object.keys(action.payload).length === 0) return initialState;
      return {
        ...state,
        ...action.payload,
      };
    },
  },
});

export const selectSession = (state: RootState) => state.session;

export const { setSessionUrl } = sessionSlice.actions;

export default sessionSlice.reducer;
