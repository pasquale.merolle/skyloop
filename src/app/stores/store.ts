import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import searchFormReducer from './SearchForm';
import sessionReducer from './Session';
import barStatusReducer from './BarStatus';
import stopsReducer from './Stops';
import { setupListeners } from '@reduxjs/toolkit/query'
import { skyscannerApi } from '../api'

export const store = configureStore({
  reducer: {
    searchForm: searchFormReducer,
    session: sessionReducer,
    barStatus: barStatusReducer,
    stops: stopsReducer,
    [skyscannerApi.reducerPath]: skyscannerApi.reducer,
  },
  // Adding the api middleware enables caching, invalidation, polling,
  // and other useful features of `rtk-query`.
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(skyscannerApi.middleware),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

// optional, but required for refetchOnFocus/refetchOnReconnect behaviors
// see `setupListeners` docs - takes an optional callback as the 2nd arg for customization
setupListeners(store.dispatch)