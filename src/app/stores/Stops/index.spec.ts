import sessionReducer, { StopsState, checkStop } from ".";

describe("stops reducer", () => {
  const initialState: StopsState = {
    checked: [0, 1, 2],
  };
  const emptyState: StopsState = {
    checked: [],
  };
  it("should handle initial state", () => {
    expect(sessionReducer(undefined, { type: "unknown" })).toEqual({
      checked: [0, 1, 2],
    });
  });

  it("should remove check stop zero", () => {
    const actual = sessionReducer(initialState, checkStop(0));
    expect(actual.checked).toEqual([1, 2]);
  });

  it("should remove check stop one", () => {
    const actual = sessionReducer(initialState, checkStop(1));
    expect(actual.checked).toEqual([0, 2]);
  });

  it("should add check stop one", () => {
    const actual = sessionReducer(emptyState, checkStop(1));
    expect(actual.checked).toEqual([1]);
  });
});
