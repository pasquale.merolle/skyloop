import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Stop } from "../../beans";
import { RootState } from "../store";

export interface StopsState {
  checked: Stop[];
}

const initialState: StopsState = {
  checked: [0, 1, 2],
};

export const stopsSlice = createSlice({
  name: "stops",
  initialState,
  reducers: {
    checkStop: (state, action: PayloadAction<Stop>) => {
      if (state.checked.includes(action.payload)) {
        state.checked = state.checked.filter(
          (check) => check !== action.payload
        );
      } else {
        state.checked.push(action.payload);
      }
    },
  },
});

export const selectStops = (state: RootState) => state.stops;

export const { checkStop } = stopsSlice.actions;

export default stopsSlice.reducer;
