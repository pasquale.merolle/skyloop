import React, { useEffect} from "react";
import { useHistory } from "react-router";
import { useGetItinerariesQuery } from "../../api";
import { FlightBound } from "../../components/FlightBound";
import { useAppSelector } from "../../hooks/hooks";
import { selectSession } from "../../stores/Session";
import styles from "./index.module.css";

export function FlightDetail() {
  const sessionState = useAppSelector(selectSession);
  const history = useHistory();

  const itineraryId = history.location.pathname
    .slice()
    .replace(/\/results\//g, "");

  const { itinerary } = useGetItinerariesQuery(sessionState.sessionUrl, {
    selectFromResult: ({ data }) => ({
      itinerary: data?.find((itinerary) => itinerary.id === itineraryId),
    }),
  });

  useEffect(()=> {
    if (!itinerary) history.push('/results')
  })
    

  return (
    <div className={styles.wrapper}>
      
      {itinerary && (
        <>
          <FlightBound bound={itinerary.outBound} />
          {itinerary.inBound && <FlightBound bound={itinerary.inBound} />}
        </>
      )}
    </div>
  );
}
