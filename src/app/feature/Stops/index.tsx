import React from "react";
import { Prices, Stop } from "../../beans";
import { StopCheckbox } from "../../components/StopsCheckbox";
import { StopLabel } from "../../components/StopsLabel";
import { useAppDispatch, useAppSelector } from "../../hooks/hooks";
import { checkStop, selectStops } from "../../stores/Stops";
import styles from "./index.module.css";

type Props = {
  prices: Prices
}

export function Stops(props: Props) {
  const stopsState = useAppSelector(selectStops);
  const dispatch = useAppDispatch();

  const handleOnChange = (id: Stop) => {
    dispatch(checkStop(id))
  }

  return (
    <div className={styles.wrap}>
      <StopLabel />
      <div className={styles.stops}>
        <StopCheckbox
          id={0} 
          name="direct" 
          label="Direct" 
          price={props.prices.zero}
          checked={stopsState.checked.includes(0)}
          onChange={(id) => handleOnChange(id)}
        />
        <StopCheckbox
          id={1}
          name="one-stop"
          label="1 stop"
          price={props.prices.one}
          checked={stopsState.checked.includes(1)}
          onChange={(id) => handleOnChange(id)}
        />
        <StopCheckbox
          id={2}
          name="two-stops"
          label="2+ stops"
          price={props.prices.two}
          checked={stopsState.checked.includes(2)}
          onChange={(id) => handleOnChange(id)}
        />
      </div>
    </div>
  );
}
