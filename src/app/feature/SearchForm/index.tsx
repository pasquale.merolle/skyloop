import React, { FormEvent, useEffect, useState } from "react";
import { SearchArrows } from "../../components/SearchArrows";
import { SearchButton } from "../../components/SearchButton";
import { SearchInput } from "../../components/SearchInput";
import { SearchSelect } from "../../components/SearchSelect";
import { useAppSelector, useAppDispatch } from "../../hooks/hooks";
import { useHistory } from "react-router-dom";
import { add, selectSearchForm } from "../../stores/SearchForm";
import { selectSession, setSessionUrl } from "../../stores/Session";

import styles from "./index.module.css";
import { DetailDestination } from "../../components/DetailDestination";

import {
  useGetPlacesQuery,
  getPlaces,
  usePostSessionMutation,
} from "../../api";
import { mapFormValuesToSessionPayload, validateFields } from "../../utils";
import { FormCheck } from "../../beans";
import { SearchError } from "../../components/SearchError";
import { selectBarStatus } from "../../stores/BarStatus";
import { HomeHeading } from "../../components/HomeHeading";

export function SearchForm() {
  const formValues = useAppSelector(selectSearchForm);
  const barStatusState = useAppSelector(selectBarStatus);
  const sessionDataState = useAppSelector(selectSession);
  const history = useHistory();
  const dispatch = useAppDispatch();

  const [query, setQuery] = useState<string>("");
  const [check, setCheck] = useState<FormCheck[]>([
    { valid: true, field: "", message: "" },
  ]);
  const [show, setShow] = useState<{ to: boolean; from: boolean }>({
    to: false,
    from: false,
  });
  const { data: placesData } = useGetPlacesQuery(query);
  const [postSession, { data: sessionData }] = usePostSessionMutation();

  useEffect(() => {
    if (sessionData && !sessionDataState.sessionUrl) {
      dispatch(setSessionUrl(sessionData));
      history.push("/results");
    }
  });

  const addToForm = (
    key: "type" | "from" | "to" | "departDate" | "returnDate",
    value: string
  ) => {
    dispatch(add({ ...formValues, [key]: value }));
  };

  const switchFromTo = () => {
    dispatch(add({ ...formValues, from: formValues.to, to: formValues.from }));
  };

  const handleOneWayFlight = () => {
    dispatch(add({ ...formValues, type: "oneway", returnDate: "" }));
  };

  const handlePlaceChange = (key: "from" | "to", place: string) => {
    setQuery(place);
    dispatch(getPlaces(place));
    addToForm(key, place);
  };

  const handleFormSubmit = (e: FormEvent) => {
    e.preventDefault();
    const check = validateFields(formValues);
    setCheck(check);
    dispatch(setSessionUrl({}));
    if (!check.length) postSession(mapFormValuesToSessionPayload(formValues));
  };

  const showSelect = (key: string, value: boolean) => {
    setShow({
      ...show,
      [key]: value,
    });
  };

  const handleOnSelectedItem = (
    key: "to" | "from",
    label: string,
    value: string
  ) => {
    addToForm(key, label);
    showSelect(key, false);
  };

  const handleOnInputFocus = (key: "to" | "from") => {
    setCheck([{ valid: true, field: "", message: "" }])
    showSelect(key, false);
  };

  return (
    <div className={`${styles.wrapper} ${barStatusState.barStatus !== "home" && styles.relative}`}>
      {barStatusState.barStatus === "home" && <HomeHeading />}
      <form
        className={`${styles.form} ${barStatusState.barStatus !== "home" && styles.relative}`}
        autoComplete="off"
        action=""
        onSubmit={(e) => handleFormSubmit(e)}
      >
        <div
          className={`${styles.bar} ${
            barStatusState.barStatus === "details" && styles.results
          }`}
        >
          <div className={styles.options}>
            <SearchInput
              variant="radio"
              name="flight"
              value="return"
              label="Return"
              onChange={(value) => addToForm("type", value)}
              checked={formValues.type === "return"}
              check={check}
            />
            <SearchInput
              variant="radio"
              name="flight"
              value="oneway"
              label="One Way"
              onChange={(value) => handleOneWayFlight()}
              checked={formValues.type === "oneway"}
              check={check}
            />
          </div>
          <div className={styles.fields}>
            <div className={styles.legs}>
              <SearchInput
                showSelect={show.from}
                setShowSelect={(show) => showSelect("from", show)}
                variant="text"
                name="from"
                value={formValues.from}
                label="Fro"
                checked={true}
                disabled={false}
                onChange={(value) => handlePlaceChange("from", value)}
                onFocus={() => handleOnInputFocus("to")}
                check={check}
              >
                {placesData && (
                  <SearchSelect
                    onSelectedItem={(label, value) =>
                      handleOnSelectedItem("from", label, value)
                    }
                    boldValue={formValues.from}
                    items={placesData}
                  />
                )}
              </SearchInput>
              <span className={styles.arrows}>
                <SearchArrows onClick={() => switchFromTo()} />
              </span>
              <SearchInput
                showSelect={show.to}
                setShowSelect={(show) => showSelect("to", show)}
                variant="text"
                name="to"
                value={formValues.to}
                label="To"
                checked={true}
                disabled={false}
                onChange={(value) => handlePlaceChange("to", value)}
                onFocus={() => handleOnInputFocus("from")}
                check={check}
              >
                {placesData && (
                  <SearchSelect
                    onSelectedItem={(label, value) =>
                      handleOnSelectedItem("to", label, value)
                    }
                    boldValue={formValues.to}
                    items={placesData}
                  />
                )}
              </SearchInput>
              <SearchInput
                variant="date"
                name="depart"
                value={formValues.departDate}
                label="Depart"
                checked={true}
                disabled={false}
                onChange={(value) => addToForm("departDate", value)}
                onFocus={() => setCheck([{ valid: true, field: "", message: "" }])}
                check={check}
              />
              <SearchInput
                variant="date"
                name="return"
                value={formValues.returnDate}
                label="Return"
                checked={true}
                disabled={formValues.type === "oneway"}
                onChange={(value) => addToForm("returnDate", value)}
                onFocus={() => setCheck([{ valid: true, field: "", message: "" }])}
                check={check}
              />
            </div>
            <div className={styles.actions}>
              <SearchButton variant="submit" label="Search flights" />
            </div>
          </div>
          <SearchError check={check} />
        </div>
        {barStatusState.barStatus === "details" && formValues.to && (
          <DetailDestination destination={formValues.to} />
        )}
      </form>
    </div>
  );
}
