import React from "react";
import { useGetItinerariesQuery } from "../../api";
import { Prices } from "../../beans";
import { ItineraryComp } from "../../components/ItineraryComp";
import { useAppSelector } from "../../hooks/hooks";
import { selectSession } from "../../stores/Session";
import { selectStops } from "../../stores/Stops";
import { filterItineraries } from "../../utils/mapping";
import { Stops } from "../Stops";
import styles from "./index.module.css";

export function Itineraries() {
  const sessionState = useAppSelector(selectSession);
  const { checked } = useAppSelector(selectStops);

  const { isLoading, error, itineraries, isFetching } = useGetItinerariesQuery(
    sessionState.sessionUrl,
    {
      pollingInterval: 120000,
      selectFromResult: ({ data, error, isLoading, isFetching }) => ({
        itineraries: filterItineraries(data, checked),
        error,
        isLoading,
        isFetching
      }),
    }
  );

  const renderMinPrices = (): Prices => {
    const res: { stops: number | undefined; minPrice: number }[] = [];
    itineraries?.forEach((itinerary) => {
      if (itinerary.outBound.info.stops || itinerary.inBound?.info.stops) {
        const obj = {
          stops:
            itinerary.outBound.info.stops.length ||
            itinerary.inBound?.info.stops.length,
          minPrice: Math.min(...itinerary.prices),
        };
        res.push(obj);
      }
    });

    const resObj: any = { zero: [], one: [], two: [] };

    if (res.length) {
      res.forEach((r) => {
        r.stops === 0 && resObj.zero.push(r.minPrice);
        r.stops === 1 && resObj.one.push(r.minPrice);
        r.stops === 2 && resObj.two.push(r.minPrice);
      });
    }
    const zero = resObj.zero.sort((a: number, b: number) => a - b)[0];
    const one = resObj.one.sort((a: number, b: number) => a - b)[0];
    const two = resObj.two.sort((a: number, b: number) => a - b)[0];

    return { zero, one, two };
  };

  return (
    <div className={styles.itineraries}>
      {error && <div>Ops, Something went wrong</div>}
      {isLoading && <div>Loading...</div>}
      {!isLoading && (
        <Stops prices={renderMinPrices()} />
      )}
      {!isLoading && !isFetching && (!itineraries || !itineraries.length) && (
        <div>There's No Results For Your Search. Try change form request or filters</div>
      )}
      {itineraries?.map((itinerary, i) => (
        <ItineraryComp key={i} itinerary={itinerary} />
      ))}
    </div>
  );
}
