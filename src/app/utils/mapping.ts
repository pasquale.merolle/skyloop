import {
  ListItem,
  SessionPayload,
  Segment,
  Carrier,
  Itinerary,
  Bound,
  Airport,
} from "../beans";
import { API } from "../beans/api";
import { SearchFormState } from "../stores/SearchForm";

export const mapPlacesToListItems = (
  places: API.Places | undefined
): ListItem[] | undefined => {
  if (places)
    return places.Places.filter((place) => place.CityId !== "-sky").map(
      (place) => {
        const { PlaceId, PlaceName } = place;
        const value = PlaceId.split("-")[0];
        return {
          value,
          label: `${PlaceName} (${value})`,
          selected: false,
        };
      }
    );
};

export const mapFormValuesToSessionPayload = (
  formValues: SearchFormState
): SessionPayload => {
  const { from, to, departDate, returnDate, type } = formValues;
  const payload: SessionPayload = {
    country: window.navigator.language.slice(-2, 5),
    currency: "EUR",
    locale: window.navigator.language,
    locationSchema: "iata",
    originplace: from.split(/[()]/)[1].slice(0, 3),
    destinationplace: to.split(/[()]/)[1].slice(0, 3),
    outbounddate: departDate,
  };
  if (type === "return") payload["inbounddate"] = returnDate;
  return payload;
};

const renderConnection = (bound: Bound) => {
  let r: any = {};
  for (let index = 0; index < bound.segments.length; index++) {
    r[`segment${index}Departure`] = new Date(
      bound.segments[index].departure.time
    );
    r[`segment${index}Arrival`] = new Date(
      bound.segments[index].arrival.time
    );
  }

  const arr: number[] = [];
  for (const key in r) {
    if (Object.prototype.hasOwnProperty.call(r, key)) {
      const element = r[key];
      arr.push(element.getTime());
    }
  }

  let rest: any[] = [];
  if (arr.length > 2) {
    rest = arr
      .slice(1, -1)
      .map((v, i, a) => i % 2 && v - (a[i - 1] || 0))
      .filter((v) => v)
      .map((val) => {
        const dateString = new Date(val).toISOString().slice(11, 16);
        if (dateString.startsWith("00")) return dateString.slice(3) + "m";
        return dateString.replace(":", "h ");
      });
  }
  return rest;
};

export const mapPollToItineraies = (
  poll: API.Poll | undefined
): Itinerary[] | undefined => {
  if (!poll) return;

  const places: Airport[] = poll.Places.map((place) => ({
    id: place.Id,
    code: place.Code,
    name: place.Name,
  }));

  const findPlaceById = (id: number): Airport => {
    return places
      .slice()
      .filter((place) => place.id === id)
      .map((place) => ({
        id: place.id,
        code: place.code,
        name: place.name,
      }))[0];
  };

  const findPlaceByIds = (ids: number[]) => {
    return ids.map((id) => {
      return findPlaceById(id);
    });
  };

  const carriers: Carrier[] = poll.Carriers.map((carrier) => ({
    id: carrier.Id,
    code: carrier.Code,
    name: carrier.Name,
  }));

  const findCarrierByFlightNumbers = (
    flightNumbers: { CarrierId: number; FlightNumber: string }[]
  ): Carrier[] => {
    const res: Carrier[] = [];
    carriers.forEach((carrier) => {
      flightNumbers.forEach((flight) => {
        if (flight.CarrierId === carrier.id) res.push(carrier);
      });
    });
    return res;
  };

  const segments: Segment[] = poll.Segments.map((segment) => ({
    id: segment.Id,
    direction: segment.Directionality,
    departure: {
      time: segment.DepartureDateTime,
      airport: findPlaceById(segment.OriginStation),
    },
    arrival: {
      time: segment.ArrivalDateTime,
      airport: findPlaceById(segment.DestinationStation),
    },
    duration: segment.Duration,
    flight: segment.FlightNumber,
  }));

  const findSegmentsBySegmentsId = (ids: number[]): Segment[] => {
    const res: Segment[] = [];
    segments.forEach((segment) => {
      ids.forEach((id) => {
        if (id === segment.id) res.push(segment);
      });
    });
    return res;
  };

  const bounds: Bound[] = poll.Legs.map((leg) => ({
    id: leg.Id,
    direction: leg.Directionality,
    departure: {
      time: leg.Departure,
      airport: findPlaceById(leg.OriginStation),
    },
    arrival: {
      time: leg.Arrival,
      airport: findPlaceById(leg.DestinationStation),
    },
    info: {
      duration: leg.Duration,
      stops: findPlaceByIds(leg.Stops),
    },
    segments: findSegmentsBySegmentsId(leg.SegmentIds),
    flights: findCarrierByFlightNumbers(leg.FlightNumbers),
    connections: []
  }));

  const findBoundById = (id: string): Bound =>
    bounds.slice().filter((bound) => bound.id === id)[0];
  
  bounds.forEach(bound => {
    bound.connections = renderConnection(bound)
  })

  const itineraries: Itinerary[] = poll.Itineraries.map((itinerary) => ({
    id: itinerary.BookingDetailsLink.Body,
    inBound: itinerary.InboundLegId ? findBoundById(itinerary.InboundLegId) : undefined,
    outBound: findBoundById(itinerary.OutboundLegId),
    prices: itinerary.PricingOptions.map((option) => option.Price),
  }));

  return itineraries
};

export const filterItineraries = (
  itineraries: Itinerary[] | undefined,
  stopNumbers: number[]
): Itinerary[] | undefined => {
  if (itineraries) {
    let res: Itinerary[] = [];
    stopNumbers.forEach((stopNumber) => {
      const filter = itineraries?.filter((itinerary) => {
        if (itinerary.inBound) {
          if (stopNumber === 0) {
            return (
              itinerary.inBound?.info.stops.length === stopNumber &&
              itinerary.outBound.info.stops.length === stopNumber
            );
          }
          return (
            itinerary.inBound?.info.stops.length === stopNumber ||
            itinerary.outBound.info.stops.length === stopNumber
          );
        }
        return itinerary.outBound.info.stops.length === stopNumber;
      });
      res = [...res, ...filter];
    });
    return res;
  }
};