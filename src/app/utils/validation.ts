import { FormCheck } from "../beans";
import { SearchFormState } from "../stores/SearchForm";

export const validateFields = (formValues: SearchFormState): FormCheck[] => {
  const { from, to, departDate, returnDate, type } = formValues;
  const today = new Date().toISOString().split("T")[0];
  const tod = new Date(today);
  const dep = new Date(departDate);

  let check = [];
  if (!from.length)
    check.push({
      valid: false,
      field: "from",
      message: "from field is mandatory",
    });
  if (!to.length)
    check.push({
      valid: false,
      field: "to",
      message: "to field is mandatory",
    });
  if (!departDate) {
    check.push({
      valid: false,
      field: "depart",
      message: "depart date is mandatory",
    });
  }
  if (type === "return" && !returnDate) {
    check.push({
      valid: false,
      field: "return",
      message: "return date is mandatory",
    });
  }
  if (dep < tod)
    check.push({
      valid: false,
      field: "depart",
      message: "depart date must be greater or equal than today",
    });
  if (returnDate) {
    const ret = new Date(returnDate);
    if (ret < tod)
      check.push({
        valid: false,
        field: "return",
        message: "return date must be greater or equal than today",
      });
    if (ret < dep)
      check.push({
        valid: false,
        field: "return",
        message: "return date must be greater or equal than depart date",
      });
  }
  return check;
};
