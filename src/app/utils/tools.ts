export const currencyFormatter = (value: number) => {
  const str = new Intl.NumberFormat(window.navigator.language, {
    style: "currency",
    currency: "EUR",
    maximumFractionDigits: 0,
  }).format(Math.floor(value));
  return str.substr(str.length - 1) + " " + str.substr(0, str.length - 1);
};

export const convertMinsToHrsMins = (mins: number) => {
  const h = Math.floor(mins / 60);
  const hours = h < 10 ? "" + h : h;
  const minutes = mins % 60;
  if (hours === "0") return `${minutes}m`
  return `${hours}h ${minutes}`;
};

export const renderTime = (timeString: string): string =>
  timeString.split("T")[1].substring(0, 5);
