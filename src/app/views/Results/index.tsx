import React, { useEffect } from "react";
import { Itineraries } from "../../feature/Itineraries";
import { useAppDispatch } from "../../hooks/hooks";
import { setBarStatus } from "../../stores/BarStatus";
import styles from "./index.module.css";

export function Results() {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(setBarStatus('results'))
  })

  return (
    <div className={styles.bg}>
      <Itineraries />
    </div>
  );
}
