import React, { useEffect } from "react";
import { useAppDispatch } from "../../hooks/hooks";
import { setBarStatus } from "../../stores/BarStatus";
import styles from "./index.module.css";

export function Home() {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(setBarStatus("home"));
  });
  return <div className={styles.background} />;
}
