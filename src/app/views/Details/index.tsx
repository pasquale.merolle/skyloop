import React, { useEffect } from "react";
import { FlightDetail } from "../../feature/FlightDetail";
import { useAppDispatch } from "../../hooks/hooks";
import { setBarStatus } from "../../stores/BarStatus";

export function Details() {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(setBarStatus("details"));
  });

  return (
    <div>
      <FlightDetail />
    </div>
  );
}
