export type ListItem = {
  value: string;
  label: string;
  selected: boolean;
};

export type Airport = {
  id: number;
  code: string;
  name: string;
};

export type Carrier = {
  id: number;
  code: string;
  name: string;
};

export type Leg = {
  time: string;
  airport: Airport;
};

export type Segment = {
  id: number;
  direction: string;
  departure: Leg;
  arrival: Leg;
  duration: number;
  flight: string;
};

export type Info = {
  duration: number;
  stops: {
    code: string;
    name: string;
  }[];
};

export type Bound = {
  id: string;
  direction: string;
  departure: Leg;
  arrival: Leg;
  info: Info;
  segments: Segment[];
  connections: string[];
  flights: Carrier[];
};

export type Itinerary = {
  id: string;
  inBound?: Bound;
  outBound: Bound;
  prices: number[];
};

export type SessionPayload = {
  country: string;
  currency: string;
  locale: string;
  locationSchema: string;
  originplace: string;
  destinationplace: string;
  outbounddate: string;
  inbounddate?: string;
};

export type FormCheck = {
  valid: boolean;
  field: string;
  message: string;
};

export type Stop = 0 | 1 | 2;

export type Prices = {
  zero: number;
  one: number;
  two: number;
};
