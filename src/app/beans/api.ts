type SuggestPlace = {
  PlaceId: string;
  PlaceName: string;
  CountryId: string;
  RegionId: string;
  CityId: string;
  CountryName: string;
};

type Query = {
  Country: string;
  Currency: string;
  Locale: string;
  Adults: number;
  Children: number;
  Infants: number;
  OriginPlace: string;
  DestinationPlace: string;
  OutboundDate: string;
  InboundDate: string;
  LocationSchema: string;
  CabinClass: string;
  GroupPricing: boolean;
};

type PricingOption = {
  Agents: number[];
  QuoteAgeInMinutes: number;
  Price: number;
  DeeplinkUrl: string;
};

type Itinerary = {
  OutboundLegId: string;
  InboundLegId?: string;
  PricingOptions: PricingOption[];
  BookingDetailsLink: {
    Uri: string;
    Body: string;
    Method: string;
  };
};

type FlightNumber = {
  FlightNumber: string;
  CarrierId: number;
};

type Leg = {
  Id: string;
  SegmentIds: number[];
  OriginStation: number;
  DestinationStation: number;
  Departure: string;
  Arrival: string;
  Duration: number;
  JourneyMode: string;
  Stops: number[];
  Carriers: number[];
  OperatingCarriers: number[];
  Directionality: string;
  FlightNumbers: FlightNumber[];
};

type Agent = {
  Id: number;
  Name: string;
  ImageUrl: string;
  Status: string;
  OptimisedForMobile: boolean;
  Type: string;
};

type Segment = {
  Id: number;
  OriginStation: number;
  DestinationStation: number;
  DepartureDateTime: string;
  ArrivalDateTime: string;
  Carrier: number;
  OperatingCarrier: number;
  Duration: number;
  FlightNumber: string;
  JourneyMode: string;
  Directionality: string;
};

type Place = {
  Id: number;
  ParentId: number;
  Code: string;
  Type: string;
  Name: string;
};

type Carrier = {
  Id: number;
  Code: string;
  Name: string;
  ImageUrl: string;
  DisplayCode: string;
};

type Currency = {
  Code: string
  DecimalDigits: number
  DecimalSeparator: string
  RoundingCoefficient: number
  SpaceBetweenAmountAndSymbol: boolean
  Symbol: string
  SymbolOnLeft: boolean
  ThousandsSeparator: string
}
export namespace API {
  export type Places = {
    Places: SuggestPlace[];
  };
  export type Session = {
    sessionUrl: string;
  } | {};
  export type Poll = {
    SessionKey: string;
    Query: Query;
    Status: string;
    Itineraries: Itinerary[];
    Legs: Leg[];
    Agents: Agent[];
    Segments: Segment[];
    Places: Place[];
    Carriers: Carrier[];
    Currencies: Currency[]
  };
}
