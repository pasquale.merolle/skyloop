import React from "react";
import "./App.css";
import { Home } from "./app/views/Home";
import { SearchForm } from "./app/feature/SearchForm";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Results } from "./app/views/Results";
import { Details } from "./app/views/Details";

function App() {
  return (
    <Router>
      <div className="App">
        <SearchForm />
        <Switch>
          <Route path="/results/:itineraryId">
            <Details />
          </Route>
          <Route path="/results">
            <Results />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
